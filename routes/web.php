<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//primera ruta 
Route::get('Hola', function(){
echo "Hola estoy en laravel";
});

Route::get('arreglo',function(){
    //crear un arreglo de estudiantes
    $estudiantes = ["andres", "Laura", "ana", "rodrigo" ];
     var_dump($estudiantes);

     //Recorrer un arregloDB::del,['John'])
    foreach ($estudiantes as $key => $value){
              echo "$value tiene indice $key <hr />";

    }
    });

    Route::get('paises', function(){
        //crear un arreglo
        $paises = [
            "Colombia" => [ 
                "Capital" => "Bogota",
                "Moneda" => "Peso",
                "Poblacion" => 50.372
            ],
            "Ecuador" => [
                "Capital" => "Quito",
                "Moneda" => "Dolar",
                "Poblacion" => 17.517
            ],
            "Brasil" => [
                "Capital" => "Brasilia",
                "Moneda" => "Real",
                "Poblacion" => 2,481
            ],
            "Bolivia" => [
                "Capital" => "Sucre",
                "Moneda" => "Boliviano",
                "Poblacion" => 11.633
            ]


        ];

        //Recorrer la primera dimension del arreglo
        foreach ($paises as $pais => $infopais ){
            echo "<h2> $pais </h2>";
            
        }
        
    });

